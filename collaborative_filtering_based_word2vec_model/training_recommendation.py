import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from annoy import AnnoyIndex


text_file = open('data/altroz_data_for_Recys_unique_all_data.txt', "r")
lines = text_file.readlines()
train = [line[:-1] for line in lines]

data_for_embeddings = []
for j in train:
    token_list = j.split()
    if len(token_list) <=3:
        pass
    else:
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i+1]
            data_for_embeddings.append(" ".join(n_gram_sequence))
data = pd.DataFrame({"sentences":data_for_embeddings})
data.to_excel("data/data_for_embeddings.xlsx")
D = 100
window = 2
model_vec = Word2Vec(data["sentences"].str.split(), size=D, window=window, min_count=1, workers=4)
model_vec.save('models/altroz_word2vec_latest_embed_4th_august.vec')


def find_sent_embedding(sentence):
    sum_of_word_vectors = np.zeros(int(D))
    no_of_words = 0
    for word in sentence.split(): 
        no_of_words += 1
        try:
            sum_of_word_vectors = sum_of_word_vectors + model_vec.wv[word]
        except:
            empty_vector = np.zeros(int(D))
            sum_of_word_vectors = sum_of_word_vectors + empty_vector
            no_of_words -=1
    avg_of_word_vec = (sum_of_word_vectors/no_of_words)
    return avg_of_word_vec


NUM_TREES=10
ann = AnnoyIndex(D,'angular')
for index, embed in enumerate(data_for_embeddings):
    vec = find_sent_embedding(embed)
    ann.add_item(index, vec)
ann.build(NUM_TREES)
ann.save('models/annoy_vec.index')

