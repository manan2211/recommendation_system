import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from annoy import AnnoyIndex


D=100
ann = AnnoyIndex(D,'angular')
ann.load('models/annoy_vec.index')
model_vec = Word2Vec.load('models/altroz_word2vec_latest_embed_4th_august.vec')
data_for_embeddings = pd.read_excel("data/data_for_embeddings.xlsx")
data = data_for_embeddings["sentences"].to_list()


def find_sent_embedding(sentence):
    sum_of_word_vectors = np.zeros(int(D))
    no_of_words = 0
    for word in sentence.split(): 
        no_of_words += 1
        try:
            sum_of_word_vectors = sum_of_word_vectors + model_vec.wv[word]
        except:
            empty_vector = np.zeros(int(D))
            sum_of_word_vectors = sum_of_word_vectors + empty_vector
            no_of_words -=1
    avg_of_word_vec = (sum_of_word_vectors/no_of_words)
    return avg_of_word_vec


def predict_next(text,no_of_similar_sentences,no_of_predictions):
    vec_num = ann.get_nns_by_vector(find_sent_embedding(text),no_of_similar_sentences)
    sentences = []
    for i in vec_num:
        sentences.append(data[i])
    suggestions = ""
    for sent in sentences:
        suggestions = suggestions + " " + sent
    unique_suggestions = set(suggestions.split())
    last_user_input = text.split()[-1]
    sim_dict = {}
    for i in unique_suggestions:
        sim = model_vec.wv.similarity(i,last_user_input)
        sim_dict[i] = sim
    sort_orders_keys = [x[0] for x in sorted(sim_dict.items(), key=lambda x: x[1], reverse=True)]
    return sort_orders_keys[:no_of_predictions]


def predict_next_discard_already_occured_intents(text,no_of_similar_sentences,no_of_predictions):
    vec_num = ann.get_nns_by_vector(find_sent_embedding(text),no_of_similar_sentences)
    sentences = []
    for i in vec_num:
        sentences.append(input_seq_new[i])
    suggestions = ""
    for sent in sentences:
        suggestions = suggestions + " " + sent
    unique_suggestions = set(suggestions.split())
    inp = text.split()
    recomm2 = [x for x in unique_suggestions if x not in inp]
    last_user_input = text.split()[-1]
    sim_dict = {}
    for i in recomm2:
        sim = model_new.wv.similarity(i,last_user_input)
        sim_dict[i] = sim
    sort_orders_keys = [x[0] for x in sorted(sim_dict.items(), key=lambda x: x[1], reverse=True)]
    return sort_orders_keys[:no_of_predictions]

