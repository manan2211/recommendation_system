import pandas as pd
import numpy as np
from annoy import AnnoyIndex
import fasttext
from sklearn.metrics.pairwise import cosine_similarity

D=100
ann = AnnoyIndex(D,'angular')
ann.load('models/annoy_vec.index')
model_ft = fasttext.load_model("models/model_ft.bin")
data = pd.read_csv("data/sequence_data.csv")

input_s = data["input_seq"].to_list()
label_s = data["label"].to_list()

session_seq = []
for i in zip(input_s,label_s):
    inner = i[0]+" "+i[1]
    session_seq.append(inner)

def find_sent_embedding(sentence):
    sum_of_word_vectors = np.zeros(int(D))
    no_of_words = 0
    for word in sentence.split(): 
        no_of_words += 1
        try:
            sum_of_word_vectors = sum_of_word_vectors + model_ft.get_word_vector(word)
        except:
            empty_vector = np.zeros(int(D))
            sum_of_word_vectors = sum_of_word_vectors + empty_vector
            no_of_words -=1
    avg_of_word_vec = (sum_of_word_vectors/no_of_words)
    return avg_of_word_vec

def get_cosine_similarity(feature_vec_1, feature_vec_2):    
    return cosine_similarity(feature_vec_1.reshape(1, -1), feature_vec_2.reshape(1, -1))[0][0]

def predict_next(text,no_of_similar_sentences,no_of_predictions):
    vec_num = ann.get_nns_by_vector(find_sent_embedding(text),no_of_similar_sentences)
    sentences = []
    for i in vec_num:
        sentences.append(session_seq[i])
    suggestions = ""
    for sent in sentences:
        suggestions = suggestions + " " + sent
    unique_suggestions = set(suggestions.split())
    inp = text.split()
    recomm2 = [x for x in unique_suggestions if x not in inp]
    last_user_input = text.split()[-1]
    sim_dict = {}
    for i in recomm2:
        sim = get_cosine_similarity(model_ft.get_word_vector(i),find_sent_embedding(text))
        sim_dict[i] = sim
    sort_orders_keys = [x[0] for x in sorted(sim_dict.items(), key=lambda x: x[1], reverse=True)]
    return sort_orders_keys[:no_of_predictions]
