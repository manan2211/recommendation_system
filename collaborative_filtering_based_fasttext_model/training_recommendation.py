import pandas as pd
import numpy as np
from annoy import AnnoyIndex
from sklearn.utils import shuffle
import fasttext
from sklearn.metrics.pairwise import cosine_similarity
D = 100

text_file = open('data/altroz_data_for_Recys_unique_all_data.txt', "r")
lines = text_file.readlines()
train = [line[:-1] for line in lines]

# creating input output data
input_seq = []
label = []
for j in train:
    token_list = j.split()
    if len(token_list) <=3:
        pass
    else:
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i+1]
            if len(n_gram_sequence) == (len(token_list)):
                break
            else:
                input_seq.append(" ".join(n_gram_sequence))
                label.append(token_list[i+1])
                
df = pd.DataFrame({"input_seq":input_seq,"label":label})
df.drop_duplicates(inplace=True)
df = shuffle(df)
df.to_csv("data/sequence_data.csv")
input_s = df["input_seq"].to_list()

model_ft = fasttext.train_supervised(input="data/sequence_data.csv")
model_ft.save_model("models/model_ft.bin")
model_ft = fasttext.load_model("models/model_ft.bin")

def find_sent_embedding(sentence):
    sum_of_word_vectors = np.zeros(int(D))
    no_of_words = 0
    for word in sentence.split(): 
        no_of_words += 1
        try:
            sum_of_word_vectors = sum_of_word_vectors + model_ft.get_word_vector(word)
        except:
            empty_vector = np.zeros(int(D))
            sum_of_word_vectors = sum_of_word_vectors + empty_vector
            no_of_words -=1
    avg_of_word_vec = (sum_of_word_vectors/no_of_words)
    return avg_of_word_vec

NUM_TREES=10
ann = AnnoyIndex(D,'angular')
for index, embed in enumerate(input_s):
    vec = find_sent_embedding(embed)
    ann.add_item(index, vec)
ann.build(NUM_TREES)
ann.save('models/annoy_vec.index')

