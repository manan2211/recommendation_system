import pandas as pd
import gensim 
import numpy as np
import shutil
import csv
from numpy import array
from numpy import asarray
from numpy import zeros
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, TimeDistributed, Conv1D, MaxPooling1D
from keras.utils import to_categorical
from gensim.models.fasttext import FastText
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding, LSTM, Dense, Dropout
from keras.preprocessing.text import Tokenizer
from keras.callbacks import EarlyStopping
from keras.models import Sequential
import keras.utils as ku 
import numpy as np
tokenizer = Tokenizer()
from tensorflow.keras.models import load_model

new_model=load_model('/home/manan/Maruti_recommender_system/recommendation_system/sequential_method/models/recommend_v1_sequential_bi-lstm.h5')
new_model.summary()

df= pd.read_csv('data/final_sequence_data.csv')
#df.shape
t = Tokenizer()
t.fit_on_texts(df['text'])
word_index = t.word_index

input_seq = []
for i in df['text']:
    input_seq.append(i.split()) 

max_len = max([len(x) for x in input_seq])

uniq_label_list = df['label'].unique().tolist()
#print(uniq_label_list)

def recommend_tag():
    seed_text = ""
    while exit:
        inp = input("ENTER TAG: ")
        if inp == "exit":
            break
        else:
            seed_text += " " + inp
            token_list = t.texts_to_sequences([seed_text])
            token_list = pad_sequences(token_list, maxlen=max_len, padding='pre')
            pred = new_model.predict(token_list)
            indx_list = np.argsort(pred[0])[::-1]
            pred_labels = [uniq_label_list[x] for x in indx_list]
            res = []
            for i in pred_labels:
                if i not in seed_text.split():
                    res.append(i)
                    if len(res) == 3:
                        break
            print("SEQUENCE:  ",seed_text)
            print("RECOMMENDATION:  ",res)  

recommend_tag()


