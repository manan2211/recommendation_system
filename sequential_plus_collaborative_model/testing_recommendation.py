import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from annoy import AnnoyIndex
from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer()
from tensorflow.keras.models import load_model
from sklearn.metrics.pairwise import cosine_similarity


df = pd.read_csv('data/final_sequence_data.csv')
data = df["text"].to_list()

input_seq = []
for i in df['text']:
    input_seq.append(i.split()) 
    
t = Tokenizer()
t.fit_on_texts(df['text'])
vocab_size = len(t.word_index) + 1
word_index = t.word_index
D = 100

new_model=load_model('models/recommend_v1_sequential_collaborative.h5')
embeddings = new_model.layers[0].get_weights()[0]
words_embeddings = {w:embeddings[idx] for w, idx in word_index.items()}

def find_sent_embedding(sentence):
    sum_of_word_vectors = np.zeros(int(D))
    no_of_words = 0
    for word in sentence.split(): 
        no_of_words += 1
        try:
            sum_of_word_vectors = sum_of_word_vectors + words_embeddings[word.lower()]
        except:
            empty_vector = np.zeros(int(D))
            sum_of_word_vectors = sum_of_word_vectors + empty_vector
            no_of_words -=1
    avg_of_word_vec = (sum_of_word_vectors/no_of_words)
    return avg_of_word_vec

def get_cosine_similarity(feature_vec_1, feature_vec_2):    
    return cosine_similarity(feature_vec_1.reshape(1, -1), feature_vec_2.reshape(1, -1))[0][0]


NUM_TREES=10
ann = AnnoyIndex(D,'angular')
for index, embed in enumerate(data):
    vec = find_sent_embedding(embed)
    ann.add_item(index, vec)
ann.build(NUM_TREES)
ann.save('models/annoy_vec.index')


def predict_next(text,no_of_similar_sentences,no_of_predictions):
    vec_num = ann.get_nns_by_vector(find_sent_embedding(text),no_of_similar_sentences)
    sentences = []
    for i in vec_num:
        sentences.append(data[i])
    suggestions = ""
    for sent in sentences:
        suggestions = suggestions + " " + sent
    unique_suggestions = set(suggestions.split())
    inp = text.split()
    recomm2 = [x for x in unique_suggestions if x not in inp]
    last_user_input = text.split()[-1]
    sim_dict = {}
    for i in recomm2:
        sim = get_cosine_similarity(words_embeddings[i.lower()],find_sent_embedding(text))
        sim_dict[i] = sim
    sort_orders_keys = [x[0] for x in sorted(sim_dict.items(), key=lambda x: x[1], reverse=True)]
    return sort_orders_keys[:no_of_predictions]



