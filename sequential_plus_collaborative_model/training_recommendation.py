import pandas as pd
import gensim 
import numpy as np
import shutil
import csv
from numpy import array
from numpy import asarray
from numpy import zeros
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, TimeDistributed, Conv1D, MaxPooling1D
from keras.utils import to_categorical
from gensim.models.fasttext import FastText
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding, LSTM, Dense, Dropout
from keras.preprocessing.text import Tokenizer
from keras.callbacks import EarlyStopping
from keras.models import Sequential
import keras.utils as ku 
import numpy as np
tokenizer = Tokenizer()
from tensorflow.keras.models import load_model
from sklearn.utils import shuffle


#PREPARING SEQUENTIAL DATA
def listToString(s):      
    str1 = ""   
    for ele in s:  
        str1 += ele
        str1 += " "   
    # return string   
    return str1
def stringToList(string): 
    li = list(string.split(" ")) 
    return li
F = open('data/altroz_data_for_Recys_unique_all_data.txt','r', encoding='utf-8') 
l = []
for line in F:
    l.append(line.split(' ',-1))
with open('data/sequence_data.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["text","label"])
    for i in l:
        words = []
        i_str = listToString(i)
        i_str = i_str.strip()
        i = stringToList(i_str)
        if len(i)>3:
            for p in range(2):
                words.append(i[p])
            for p in range(2,len(i)):
                words.append(i[p])
                sentence = [word for word in words[:-1]]
                sent = ' '.join([str(elem) for elem in sentence])
                writer.writerow([sent,str(words[-1])])

df = pd.read_csv('data/sequence_data.csv')
#df.shape
df.drop_duplicates(inplace=True)
df = shuffle(df)
df.to_csv('data/final_sequence_data.csv')

input_seq = []
for i in df['text']:
    input_seq.append(i.split()) 

# PREPARING "X"
t = Tokenizer()
t.fit_on_texts(df['text'])
vocab_size = len(t.word_index) + 1
word_index = t.word_index
encoded_docs = t.texts_to_sequences(df['text'])
max_len = max([len(x) for x in input_seq])
padded_docs = pad_sequences(encoded_docs, maxlen=max_len, padding='pre')
#print(word_index)
#print(max_len)

# PREPARING "Y"
label = df['label']
uniq_label_list = df['label'].unique().tolist()
label_encoder = {}
count = 0
for i in uniq_label_list :
    label_encoder[i] = count
    count +=1   
y_encoded = []
for i in label:
    y_encoded.append(label_encoder[i])
label_encoded_array = np.array(y_encoded) 
num_classes=len(uniq_label_list)
y = to_categorical(y_encoded, num_classes)

#BUILIDNG SEQUENTIAL MODEL
def model_sequential():
	model = Sequential()
	model.add(Embedding(vocab_size, 100, input_length=max_len))
	model.add(LSTM(150, dropout=0.3,return_sequences = True))
	model.add(LSTM(100))
	model.add(Dense(num_classes, activation='softmax'))
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	model.fit(padded_docs, y, batch_size=64,epochs=1, verbose=1)
	model.save('models/recommend_v1_sequential_collaborative.h5')	

model_sequential()
		
